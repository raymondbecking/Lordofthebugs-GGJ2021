using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;

public class followraycast : MonoBehaviour
{
    public UnityEvent OnDied;

    public LightRayCast followObject;
    public EyeBehaviour eyeBehaviour;

    public float maxDistance = 1;

    private float waitTime = 0;
    private Vector3 transPos;
    private SpriteRenderer renderer;
    private bool flip;
    private float transPosOldX;
    private bool arrived = true;
    private float startSpeed;
    private Vector3 minimumScale;
    public bool ending;

    public GameObject spawnPointObject;
    [SerializeField]
    private GameObject blackHole;
    [SerializeField]
    private Vector3 maxScaleBlackhole = new Vector3(7, 7, 7);
    [SerializeField]
    private Vector3 growthSpeed = new Vector3(1, 1, 1);
    [SerializeField]
    private float speed = 0.5f;
    [SerializeField]
    private float timeBlackHole = 2;

    public Vector3 TargetPos
    {
        get { return targetPos; }
        private set
        {
            bool changed = targetPos != value;
            targetPos = value;
            if (changed)
                SlowDownMovement();
        }
    }

    //public Vector3 transPos;

    private Vector3 targetPos;


    private void Start()
    {
        startSpeed = speed;
        blackHole.SetActive(false);
        minimumScale = blackHole.transform.localScale;

        renderer = gameObject.GetComponent<SpriteRenderer>();
        eyeBehaviour.deathEvent.AddListener(StartRespawn);
    }

    private void SlowDownMovement()
    {
        transPosOldX = transform.position.x;
    }

    /// <summary>
    /// Flips the sprite if the target is at an other x than transform.
    /// </summary>
    private void FlipSprite()
    {
        flip = renderer.flipX;

        if (targetPos.x > transPos.x && !flip)
        {
            speed--;
            if (speed <= 0)
            {
                renderer.flipX = true;
                speed = 0;
                waitTime = 1;
            }             
        }
        else if (targetPos.x < transPos.x && flip)
        {
            speed--;
            if (speed <= 0)
            {
                renderer.flipX = false;
                speed = 0;
                waitTime = 1;
            }         
        }         
    }

    private void StartRespawn()
    {
        StartCoroutine(Respawn());
    }

    private IEnumerator Respawn()
    {
        Vector3 spawnPoint = spawnPointObject.transform.position;
        blackHole.SetActive(true);
        float time = timeBlackHole;
        while (blackHole.transform.localScale != maxScaleBlackhole)
        {
            blackHole.transform.localScale += growthSpeed;

            if (blackHole.transform.localScale.x > 7)
                blackHole.transform.localScale = maxScaleBlackhole;
            yield return null;
        }
        OnDied?.Invoke();
        transform.position = spawnPoint;

        while (blackHole.transform.localScale != minimumScale)
        {
            blackHole.transform.localScale -= growthSpeed;

            if (blackHole.transform.localScale.x < 1)
                blackHole.transform.localScale = minimumScale;
            yield return null;
        }
        blackHole.SetActive(false);
        
    }

    void Update()
    {
        if (ending)
            return;

        TargetPos = followObject.point;
        transPos = transform.position;

        FlipSprite();

        if (Mathf.Abs(targetPos.x - transPos.x) < 0.1f)
            arrived = false;
        else
            arrived = true;

        if (arrived && Mathf.Abs(targetPos.x - transPos.x) < maxDistance)
        {
            float step = speed * Time.deltaTime;
            transform.position = Vector2.MoveTowards(transform.position, targetPos, step);
        }

        waitTime -= Time.deltaTime;

        if(waitTime < 0)
        {
            speed = startSpeed;
            waitTime = 0;
        }
    }
}
