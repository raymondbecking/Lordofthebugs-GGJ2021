using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;
using System;

public class Moonlight : MonoBehaviour
{
    public event Action OnMoonModeChange;

    [SerializeField]
    float speed = .1f;
    float targetAngle = 35f;
    Quaternion target;

    [SerializeField]
    float maxAngle = 20f;
    float maxRotateAngle, minRotateAngle;

    private float outerFocusAngleLerpStart, innerFocusAngleLerpStart;
    private float outerWideAngleLerpStart, innerWideAngleLerpStart;
    private float intensityWideLerpStart, intensityFocusLerpStart;
    private float stepWide, stepFocus;
    [SerializeField]
    private float focusSpeed, wideSpeed;

    [SerializeField]
    Light2D moonLight;
    [SerializeField]
    float wideOuterAngle, wideInnerAngle;
    [SerializeField]
    float focusOuterAngle, focusInnerAngle;
    [SerializeField]
    float wideIntensity, focusIntensity;
    [SerializeField]
    private BoxCollider2D focusCollider;
    [SerializeField]
    private CapsuleCollider2D wideCollider;
    [SerializeField]
    private followraycast blob;

    public enum MoonMode
    {
        Focus,
        Wide
    }
    private MoonMode currentMoonMode;

    public MoonMode CurrentMoonMode
    {
        get { return currentMoonMode; }

        private set
        {
            if (currentMoonMode == value) return;
            currentMoonMode = value;
            if (OnMoonModeChange != null)
                OnMoonModeChange();
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        target = new Quaternion(0, 0, 0, 0);

        maxRotateAngle = 180f + maxAngle;
        minRotateAngle = 180f - maxAngle;

        currentMoonMode = MoonMode.Focus;
    }

    // Update is called once per frame
    void Update()
    {
        SetTargetDirection();
        RotateLight();

        if (Input.GetKeyDown("space"))
        {
            ToggleMoonlightMode();
        }

        switch (currentMoonMode)
        {
            case MoonMode.Wide:
                if (moonLight.pointLightOuterAngle < wideOuterAngle)
                {
                    stepWide += Time.deltaTime * wideSpeed;
                    moonLight.pointLightOuterAngle = Mathf.Lerp(outerWideAngleLerpStart, wideOuterAngle, stepWide);
                    moonLight.intensity = Mathf.Lerp(intensityWideLerpStart, wideIntensity, stepWide);
                    //print(moonLight.pointLightOuterAngle);
                }
                else
                {
                    stepWide = 0;
                }
                return;
            case MoonMode.Focus:
                if (moonLight.pointLightOuterAngle > focusOuterAngle)
                {
                    stepFocus += Time.deltaTime * focusSpeed;
                    moonLight.pointLightOuterAngle = Mathf.Lerp(outerFocusAngleLerpStart, focusOuterAngle, stepFocus);
                    moonLight.intensity = Mathf.Lerp(intensityFocusLerpStart, focusIntensity, stepFocus);
                    //print(moonLight.pointLightOuterAngle);
                }
                else
                {
                    stepFocus = 0;
                }
                return;
        }



        //Change target direction
        void SetTargetDirection()
        {
            if (Input.GetMouseButton(0))
            {
                target = Quaternion.Euler(transform.rotation.x, transform.rotation.y, targetAngle);
            }
            if (Input.GetMouseButton(1))
            {
                target = Quaternion.Euler(transform.rotation.x, transform.rotation.y, -targetAngle);
            }
        }
        //Rotate when key/mouse is being held down
        void RotateLight()
        {
            if (Input.GetMouseButton(0) || Input.GetMouseButton(1))
            {
                if (transform.rotation.eulerAngles.z <= maxRotateAngle && transform.rotation.eulerAngles.z >= 160)
                {
                    transform.rotation = Quaternion.Slerp(transform.rotation, target, Time.deltaTime * speed);
                }
                else if (transform.rotation.eulerAngles.z > maxRotateAngle)
                {
                    transform.SetPositionAndRotation(transform.position, Quaternion.Euler(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, maxRotateAngle));
                }
                else if (transform.rotation.eulerAngles.z < minRotateAngle)
                {
                    transform.SetPositionAndRotation(transform.position, Quaternion.Euler(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, minRotateAngle));
                }

            }
        }

        //Swap moonlight state focus<->wide
        void ToggleMoonlightMode()
        {
            switch (currentMoonMode)
            {
                case MoonMode.Focus:
                    //Set light lerp starting values
                    outerWideAngleLerpStart = moonLight.pointLightOuterAngle;
                    intensityWideLerpStart = moonLight.intensity;
                    stepWide = 0;

                    moonLight.pointLightInnerAngle = wideInnerAngle;
                    currentMoonMode = MoonMode.Wide;

                    focusCollider.enabled = false;
                    wideCollider.enabled = true;
                    blob.maxDistance = 3;
                    return;
                case MoonMode.Wide:
                    //Set light lerp starting values
                    outerFocusAngleLerpStart = moonLight.pointLightOuterAngle;
                    intensityFocusLerpStart = moonLight.intensity;
                    stepFocus = 0;

                    moonLight.pointLightInnerAngle = focusInnerAngle;
                    currentMoonMode = MoonMode.Focus;

                    focusCollider.enabled = true;
                    wideCollider.enabled = false;
                    blob.maxDistance = 1;
                    return;
            }
        }
    }
}
