using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lantern : MonoBehaviour
{
    [SerializeField]
    EyeBehaviour eyeScript;
    bool blobInArea;
    [SerializeField]
    float hideDelay;
    float timer;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if (blobInArea)
        {
            timer += Time.deltaTime;
            if (timer > hideDelay)
            {
                eyeScript.HideContent();
                timer = 0;
            }
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name == "LanternCollider")
        {
            blobInArea = true;
        }
    }


void OnTriggerExit2D(Collider2D other)
    {
        if (other.name == "LanternCollider")
        {
            blobInArea = false;

        }
    }
}
