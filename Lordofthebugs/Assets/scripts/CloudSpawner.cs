using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudSpawner : MonoBehaviour
{
    [SerializeField]
    GameObject clouds;
    [SerializeField]
    float spawnDelay;
    [SerializeField]
    float respawnDelay;
    float startDelay;
    float timer;


    // Start is called before the first frame update
    void Start()
    {
        startDelay = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if(timer > spawnDelay + startDelay)
        {
            //Instantiate(clouds, new Vector3(transform.position.x, transform.position.y, 0), Quaternion.identity);
            Instantiate(clouds, transform, false);
            //print("Spawn cloud");
            timer = 0;
            startDelay = respawnDelay;
        }
    }
}
