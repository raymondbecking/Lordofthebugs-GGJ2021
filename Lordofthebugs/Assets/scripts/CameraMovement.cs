using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    float dir = 0.1f;
    [SerializeField]
    float speed;
    [SerializeField]
    Camera cam;
    [SerializeField]
    GameObject levelBorderLeft, levelBorderRight;
    float cameraWidth, cameraHeight;
    float cameraLeftSide, cameraRightSide;

    // Start is called before the first frame update
    void Start()
    {
        cameraHeight = 2f * cam.orthographicSize;
        cameraWidth = cameraHeight * cam.aspect;
    }

    // Update is called once per frame
    void Update()
    {
        cameraLeftSide = cam.transform.position.x - (cameraWidth / 2);
        cameraRightSide = cam.transform.position.x + (cameraWidth / 2);

        //Allow moving the camera until right border is reached
        if (cameraRightSide < levelBorderRight.transform.position.x)
        {
            if (Input.GetKey("right") || Input.GetKey("d"))
            {
                this.transform.position = new Vector3(transform.position.x + dir * speed * Time.deltaTime, transform.position.y, transform.position.z);
            }
        }
        //Allow moving the camera until left border is reached
        if (cameraLeftSide > levelBorderLeft.transform.position.x)
        {
            if (Input.GetKey("left") || Input.GetKey("a"))
            {
                this.transform.position = new Vector3(transform.position.x - dir * speed * Time.deltaTime, transform.position.y, transform.position.z);
            }
        }
    }
}
