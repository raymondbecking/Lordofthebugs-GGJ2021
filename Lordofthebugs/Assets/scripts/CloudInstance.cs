using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudInstance : MonoBehaviour
{
    [SerializeField]
    float speed;
    [SerializeField]
    float destroyDelay;
    float timer;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += Vector3.right * speed * Time.deltaTime;

        timer += Time.deltaTime;
        if (timer > destroyDelay)
        {
            Destroy(this.gameObject);
        }
    }
}
