using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Bush : MonoBehaviour
{

    public UnityEvent OnCollision;
    [SerializeField]
    private Collider2D bushCollider;


    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.name == "blob")
        {
            OnCollision?.Invoke();
        }
    }
}
  
