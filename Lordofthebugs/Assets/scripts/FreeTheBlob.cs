using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FreeTheBlob : MonoBehaviour
{

    public List<GameObject> finischPoints;
    public List<GameObject> spawnPoint;
    public followraycast Blob;
    private float speed = 1f;
    private int index = 0;
    [SerializeField]
    private GameObject spriteBlob;
    public EyeBehaviour eye;
    private bool isInPlace;
    public GameObject text;

    void Start()
    {
        text.SetActive(false);
    }


    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name == "blob" && !isInPlace)
        {
            StartCoroutine(MoveToFinish());
            isInPlace = true;
            return;
        }
    }

    private IEnumerator MoveToFinish()
    {
        Vector3 targetPos = finischPoints[index].transform.position;
        Blob.ending = true;
        while (Blob.transform.position != targetPos)
        {
            eye.HideContent();
            float step = speed * Time.deltaTime;
            Blob.transform.position = Vector3.MoveTowards(Blob.transform.position, targetPos, step);
            if (Vector3.Distance(Blob.transform.position, targetPos) < 0.1f)
            {
                if (index < finischPoints.Count -1)
                {
                    CreateNewBlob();
                    index++;
                    break;
                }
                else
                    EndGame();

                Blob.transform.position = targetPos;
            }
            yield return null;
        }
    }

    private void CreateNewBlob()
    {
        Instantiate(spriteBlob, finischPoints[index].transform.position, Quaternion.Euler(0, 180, 0));
        Blob.transform.position = spawnPoint[index+1].transform.position;
        Blob.spawnPointObject.transform.position = spawnPoint[index+1].transform.position;
        isInPlace = false;
        Blob.ending = false;
    }

    private void EndGame()
    {
        Blob.transform.position = spawnPoint[4].transform.position;
        text.SetActive(true);
    }
}
