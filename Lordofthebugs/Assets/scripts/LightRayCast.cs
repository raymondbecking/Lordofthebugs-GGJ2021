using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightRayCast : MonoBehaviour
{

    public RaycastHit2D hit;
    public Vector3 point;

    void FixedUpdate()
    {
       
            // Cast a ray straight down.
            hit = Physics2D.Raycast(transform.position, Vector2.down + new Vector2(-transform.rotation.z, Mathf.Abs(transform.rotation.z * 3)));
            //Debug.DrawRay(transform.position, (Vector2.down + new Vector2(-transform.rotation.z, Mathf.Abs(transform.rotation.z * 3))) * 10f);
     

        // If it hits something...
        if (hit.collider.name == "Terrain" || hit.collider.name == "EyeSpawner" || hit.collider.name == "blob")
        {
            //Debug.Log(hit.transform.name);
            point = hit.point;

            //if (hit.collider.isTrigger)
            //{
            //    Debug.Log(hit.transform.name);
            //}
        }
    }
}

