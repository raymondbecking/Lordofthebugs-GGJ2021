using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EyeBehaviour : MonoBehaviour
{
    public UnityEvent deathEvent;

    public followraycast blob;
    public LightRayCast followLight;
    public Moonlight moonlight;
    //public Bush bush;
    public List<Eye> eyes;

    private int index = 0;
    private bool toggle;
    private bool glowing;
    private bool isDying;

    [SerializeField]
    private bool isBeing;
    [SerializeField]
    private bool isDarkObject;
    [SerializeField]
    private bool isDarkForrest;
    [SerializeField]
    private float minOffset = -1;
    [SerializeField]
    private float maxOffset = 1;
    [SerializeField]
    private float timeBetweenSpawn = 1.5f;
    [SerializeField]
    private float timeToDespawn = 2.5f;
    [SerializeField]
    private float eyesAppear = 2.5f;
    [SerializeField]
    private float eyesHeightOffset;

    private float startTimeBetween;
    private float startToDespawn;
    private float starteyes;

    void Start()
    {
        foreach (Eye eye in eyes)
        {
            eye.offset = new Vector3(Random.Range(minOffset, maxOffset), Random.Range(minOffset, maxOffset) + eyesHeightOffset, 0);
            eye.gameObject.transform.position = gameObject.transform.position + eye.offset;
        }

        if (!isBeing)
            ShowAllEyes();

        moonlight.OnMoonModeChange += MoonModeChange;
        blob.OnDied.AddListener(HideContent);
        //bush.OnCollision.AddListener(DeathByBush);

        starteyes = eyesAppear;
        startTimeBetween = timeBetweenSpawn;
        startToDespawn = timeToDespawn;

    }

    /// <summary>
    /// Hides all the eyes.
    /// </summary>
    public void HideContent()
    {
        timeBetweenSpawn = startTimeBetween;
        eyesAppear = starteyes;
        index = 0;
        if (isBeing)
            isDying = false;
        foreach (Eye eye in eyes)
        {
            eye.gameObject.SetActive(false);
        }
    }

    /// <summary>
    /// Shows all eyes in one time.
    /// </summary>
    private void ShowAllEyes()
    {
        foreach (Eye eye in eyes)
        {
            eye.gameObject.SetActive(true);
            //Debug.Log(gameObject.name);

        }
    }

    private void DeathByBush()
    {
        foreach (Eye eye in eyes)
        {
            if (eye.isActiveAndEnabled == true)
            {
                deathEvent?.Invoke();
                break;
            }
        }
    }

    public void MoonModeChange()
    {
        toggle = true;
    }

    public void ShowingEyes()
    {
        timeBetweenSpawn -= Time.deltaTime;

        if (timeBetweenSpawn < 0 && index != eyes.Count && glowing == false)
        {
            timeBetweenSpawn = startTimeBetween;
            eyes[index].gameObject.SetActive(true);
            index++;
        }
        else if (index == eyes.Count && isBeing && !isDying)
        {
            isDying = true;
            deathEvent?.Invoke();
        }
    }

    void Update()
    {
        eyesAppear -= Time.deltaTime;

        if (eyesAppear < 0 && isBeing)
            ShowingEyes();

        if (eyesAppear < 0 && isDarkObject)
            ShowAllEyes();

        if (followLight.hit.collider.name == "EyeSpawner" || followLight.hit.collider.name == "blob")
        {
            timeToDespawn -= Time.deltaTime;
            eyesAppear = starteyes;
            glowing = true;
            if (timeToDespawn < 0 && isBeing && moonlight.CurrentMoonMode == Moonlight.MoonMode.Focus ||
                timeToDespawn < 0 && isBeing && moonlight.CurrentMoonMode == Moonlight.MoonMode.Wide)
                HideContent();
        }

        //if (followLight.hit.collider.name == bush.name)
        //{
        //    timeToDespawn -= Time.deltaTime;
        //    eyesAppear = starteyes;
        //    glowing = true;
        //    if (timeToDespawn < 0 && isDarkObject && moonlight.CurrentMoonMode == Moonlight.MoonMode.Focus)
        //        HideContent();
        //}


        else if (followLight.hit.collider.name != "EyeSpawner" || followLight.hit.collider.name != "blob" || toggle)
        {
            timeToDespawn = startToDespawn;
            toggle = false;
            glowing = false;
        }

        //for later
        /*if (isDarkForrest )
        {
            HideContent();
        }*/
    }
}
